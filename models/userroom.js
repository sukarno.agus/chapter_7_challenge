'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserRoom.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user'
      });
    }
  }
  UserRoom.init({
    user_id_player1: DataTypes.INTEGER,
    user_id_player2: DataTypes.INTEGER,
    player1_pick1: DataTypes.STRING,
    player2_pick1: DataTypes.STRING,
    player1_pick2: DataTypes.STRING,
    player2_pick2: DataTypes.STRING,
    player1_pick3: DataTypes.STRING,
    player2_pick3: DataTypes.STRING,
    userroom_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserRoom',
  });
  return UserRoom;
};