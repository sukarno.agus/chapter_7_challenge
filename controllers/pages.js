module.exports = {
  home (req, res) {
    const name = req.query.name;
    res.render("index", { title: "Traditional Games", name: name });
    // res.render("index", { title: "Traditional Games"});
  },
  
  success (req,res) {
    const user = req.query;
    console.log(user);

    res.render("index", { title: "Traditional Games", name: user.name });
  },

  login (req,res) {
    res.render("login", { title: "Login Page" })
  },

  games (req, res, next) {
    const name = req.query.name || "Player";
    console.log(name);
    res.render("games", {
      title: "Try Out The Games",
      name: name,
    });
  }
};