const UserBiodata = require('../models').UserBiodata;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return UserBiodata
      .findAll({
        include: [],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((userbiodata) => res.status(200).send(userbiodata))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return UserBiodata
      .findByPk(req.params.id, {
        include: [{
          model: User,
          as: 'user'
        }],
      })
      .then((userbiodata) => {
        if (!userbiodata) {
          return res.status(404).send({
            message: 'Biodata Not Found',
          });
        }
        return res.status(200).send(userbiodata);
      })
      .catch((error) => res.status(400).send(error));
  },

  add(req, res) {
    return UserBiodata
      .create({
        id: req.body.id,
        fullname: req.body.fullname,
        address: req.body.address,
        age: req.body.age,
        user_id: req.body.user_id,
      })
      .then((userbiodata) => res.status(201).send(userbiodata))
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return UserBiodata
      .findByPk(req.params.id)      
      .then(userbiodata => {
        if (!userbiodata) {
          return res.status(404).send({
            message: 'Biodata Not Found',
          });
        }
        return UserBiodata
          .update({
            fullname: req.body.fullname || userbiodata.fullname,
            address: req.body.address || userbiodata.address,
            age: req.body.age || userbiodata.age,
            user_id: req.body.user_id || userbiodata.user_id,
          })
          .then(() => res.status(200).send(userbiodata))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  delete(req, res) {
    return UserBiodata
      .findByPk(req.params.id)
      .then(userbiodata => {
        if (!userbiodata) {
          return res.status(400).send({
            message: 'Biodata Not Found',
          });
        }
        return userbiodata
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};