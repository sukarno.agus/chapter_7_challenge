const pages = require('./pages');
const user = require('./user');
const userbiodata = require('./userbiodata');
const userhistory = require('./userhistory');

module.exports = {
    pages,
    user,
    userbiodata,
    userhistory,
};