const UserHistory = require('../models').UserHistory;
const User = require('../models').User;

module.exports = {
  list(req, res) {
    return UserHistory
      .findAll({
        include: [],
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((userhistories) => res.status(200).send(userhistories))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return UserHistory
      .findByPk(req.params.id, {
        include: [{
          model: User,
          as: 'user'
        }],
      })
      .then((userhistory) => {
        if (!userhistory) {
          return res.status(404).send({
            message: 'History Not Found',
          });
        }
        return res.status(200).send(userhistory);
      })
      .catch((error) => res.status(400).send(error));
  },

  add(req, res) {
    return UserHistory
      .create({
        id: req.body.id,
        user_id: req.body.user_id,
        score: req.body.score,
      })
      .then((userhistory) => res.status(201).send(userhistory))
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return UserHistory
      .findByPk(req.params.id)      
      .then(userhistory => {
        if (!userhistory) {
          return res.status(404).send({
            message: 'History Not Found',
          });
        }
        return UserHistory
          .update({
            user_id: req.body.user_id || userhistory.user_id,
            score: req.body.score || userhistory.score,
          })
          .then(() => res.status(200).send(userhistory))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  delete(req, res) {
    return UserHistory
      .findByPk(req.params.id)
      .then(userhistory => {
        if (!userhistory) {
          return res.status(400).send({
            message: 'History Not Found',
          });
        }
        return userhistory
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};