const express = require("express");
const router = express.Router();
const pages = require('../controllers').pages;

router.get("/", pages.login);
router.post("/", pages.success);

module.exports = router;
