const express = require("express");
const router = express.Router();
const pages = require('../controllers').pages;

router.get("/", pages.games);

module.exports = router;
