const router = require("express").Router();
const userbiodataController = require('../controllers').userbiodata;

/* UserBiodata Router */
router.get('/', userbiodataController.list);
router.get('/:id', userbiodataController.getById);
router.post('/', userbiodataController.add);
router.put('/:id', userbiodataController.update);
router.delete('/:id', userbiodataController.delete);

module.exports = router;