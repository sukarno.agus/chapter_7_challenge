const express = require("express");
const router = express.Router();

const pageRouter = require("./pages");
const userRouter = require("./user");
const roomRouter = require("./room");
const userBiodataRouter = require("./userbiodata");
const userHistoryRouter = require("./userhistory");
const registerRouter = require("./login");
const loginRouter = require("./login");
const gamesRouter = require("./games");


router.use(pageRouter);
router.use("/register", registerRouter);  
router.use("/login", loginRouter); 
router.use("/games", gamesRouter); 

router.use("/api/user", userRouter); 
router.use("/api/room", roomRouter); 
router.use("/api/userbiodata", userBiodataRouter); 
router.use("/api/userhistory", userHistoryRouter); 


module.exports = router;
