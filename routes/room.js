const router = require("express").Router();
const roomController = require('../controllers').room;

/* User Router */
router.get('/', roomController.list);
router.get('/:id', roomController.getById);
router.post('/', roomController.add);
router.put('/:id', roomController.update);
router.delete('/:id', roomController.delete);

module.exports = router;