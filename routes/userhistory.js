const router = require("express").Router();
const userhistoryController = require('../controllers').userhistory;

/* UserHistory Router */
router.get('/', userhistoryController.list);
router.get('/:id', userhistoryController.getById);
router.post('/', userhistoryController.add);
router.put('/:id', userhistoryController.update);
router.delete('/:id', userhistoryController.delete);

module.exports = router;