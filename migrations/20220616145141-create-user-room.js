'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserRooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id_player1: {
        type: Sequelize.INTEGER
      },
      user_id_player2: {
        type: Sequelize.INTEGER
      },
      player1_pick1: {
        type: Sequelize.STRING
      },
      player2_pick1: {
        type: Sequelize.STRING
      },
      player1_pick2: {
        type: Sequelize.STRING
      },
      player2_pick2: {
        type: Sequelize.STRING
      },
      player1_pick3: {
        type: Sequelize.STRING
      },
      player2_pick3: {
        type: Sequelize.STRING
      },
      userroom_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserRooms');
  }
};